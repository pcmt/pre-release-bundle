# PCMT Pre Release Bundle for Akeneo Pim

## Key features

Pre Release Bundle is a template for creating new bundles for PCMT. Use it as a template while creating a new repository on Gitlab. 

## Development
### Running Test-Suits
The PcmtPreReleaseBundle is covered with tests and every change and addition has also to be covered with unit tests. It uses PHPUnit.

To run the tests you have to change to this project's root directory and run the following commands in your console:

```
make unit
```

### Coding style
PcmtPreReleaseBundle the coding style can be checked with Easy Coding Standard.

```
make ecs
```
